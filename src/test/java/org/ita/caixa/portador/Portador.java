package org.ita.caixa.portador;

/**
 * Portador do cartõa
 */
public interface Portador {

    public static final Integer MES_DE_ALUGUEL = 350;

    public Integer saldoInicial();

    public Integer salarioSemana();

    public Integer resgataCruzeiros();
}
