package org.ita.caixa.portador;

/**
 * Seu Madruga vai ao banco
 */
public final class SeuMadruga implements Portador {

    public Integer saldoInicial() {
        return MES_DE_ALUGUEL * 14 * -1;
    }

    public Integer salarioSemana() {
        return 150;
    }

    public Integer resgataCruzeiros() {
        return MES_DE_ALUGUEL;
    }
}
