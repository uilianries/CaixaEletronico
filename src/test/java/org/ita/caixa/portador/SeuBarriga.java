package org.ita.caixa.portador;

/**
 * Seu Barriga vai ao banco
 */
public final class SeuBarriga implements Portador {

    public Integer saldoInicial() {
        return MES_DE_ALUGUEL * 100;
    }

    public Integer salarioSemana() {
        return MES_DE_ALUGUEL;
    }

    public Integer resgataCruzeiros() {
        return MES_DE_ALUGUEL * 3;
    }
}
