package org.ita.caixa;

import org.ita.caixa.atm.CaixaEletronico;
import org.ita.caixa.exception.LeitoraCartaoException;
import org.ita.caixa.mock.MockHardware;
import org.ita.caixa.mock.MockServicoRemoto;
import org.ita.caixa.portador.Portador;
import org.ita.caixa.portador.SeuBarriga;
import org.ita.caixa.portador.SeuMadruga;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Testes para Caixa Eletronico
 */
public class CaixaEletronicoTest {
    private MockHardware hardware;
    private MockServicoRemoto servicoRemoto;
    private CaixaEletronico caixaEletronico;
    private Portador seuBarriga = new SeuBarriga();
    private Portador seuMadruga = new SeuMadruga();

    @Before
    public void setUp() throws LeitoraCartaoException {
        this.hardware = new MockHardware();
        this.servicoRemoto = new MockServicoRemoto(this.hardware.pegarNumeroDaContaCartao());
        this.caixaEletronico = new CaixaEletronico(this.hardware, this.servicoRemoto);
        this.caixaEletronico.logar();
    }

    @Test
    public void realizaLogin() {
        assertEquals("Usuário Autenticado", this.caixaEletronico.logar());
    }

    @Test
    public void loginUsuarioInexistente() {
        this.servicoRemoto.setNumeroConta("");
        assertEquals("Não foi possível autenticar o usuário", this.caixaEletronico.logar());
    }

    @Test
    public void loginTravaLeitora() {
        this.hardware.setHardwareException(true);
        assertEquals("Ocorreu um erro durante o login: Cartão clonado", this.caixaEletronico.logar());
    }

    @Test
    public void realizaConsultaZerada() {
        assertEquals("O saldo é R$ 0,00", this.caixaEletronico.saldo());
    }

    @Test
    public void seuBarrigaConsultaSaldo() {
        this.servicoRemoto.setSaldoInicial(seuBarriga.saldoInicial());
        this.caixaEletronico.logar();
        assertEquals("O saldo é R$ " + seuBarriga.saldoInicial() + ",00", this.caixaEletronico.saldo());
    }

    @Test
    public void seuMadrugaConsultaSaldo() {
        this.servicoRemoto.setSaldoInicial(seuMadruga.saldoInicial());
        this.caixaEletronico.logar();
        assertEquals("O saldo é R$ " + seuMadruga.saldoInicial() + ",00", this.caixaEletronico.saldo());
    }

    @Test
    public void seuBarrigaRealizaSaque() {
        this.servicoRemoto.setSaldoInicial(seuBarriga.saldoInicial());
        this.caixaEletronico.logar();
        assertEquals("Retire seu dinheiro", this.caixaEletronico.sacar(seuBarriga.resgataCruzeiros()));
        Integer diferenca = seuBarriga.saldoInicial() - seuBarriga.resgataCruzeiros();
        assertEquals("O saldo é R$ " + diferenca + ",00", this.caixaEletronico.saldo());
    }

    @Test
    public void seuMadrugaRealizaSaque() {
        this.servicoRemoto.setSaldoInicial(seuMadruga.saldoInicial());
        this.caixaEletronico.logar();
        assertEquals("Saldo insuficiente", this.caixaEletronico.sacar(seuMadruga.resgataCruzeiros()));
    }

    @Test
    public void seuBarrigaTravaDispensador() {
        this.servicoRemoto.setSaldoInicial(seuBarriga.saldoInicial());
        this.caixaEletronico.logar();
        this.hardware.setHardwareException(true);
        assertEquals("Ocorreu um erro durante o saque: Sem cédulas", this.caixaEletronico.sacar(seuBarriga.resgataCruzeiros()));
        assertEquals("O saldo é R$ " + seuBarriga.saldoInicial() + ",00", this.caixaEletronico.saldo());
    }

    @Test
    public void seuBarrigaRealizaDeposito() {
        assertEquals("Depósito recebido com sucesso", this.caixaEletronico.depositar(seuBarriga.salarioSemana()));
        assertEquals("O saldo é R$ " + seuBarriga.salarioSemana() + ",00", this.caixaEletronico.saldo());
    }

    @Test
    public void seuMadrugaTravaDeposito() {
        this.servicoRemoto.setSaldoInicial(seuMadruga.saldoInicial());
        this.caixaEletronico.logar();
        this.hardware.setHardwareException(true);
        assertEquals("Ocorreu um erro durante o depósito: Envelope vazio", this.caixaEletronico.depositar(seuMadruga.salarioSemana()));
        assertEquals("O saldo é R$ " + seuMadruga.saldoInicial() + ",00", this.caixaEletronico.saldo());
    }
}
