package org.ita.caixa.mock;

import org.ita.caixa.servico.ContaCorrente;
import org.ita.caixa.servico.ServicoRemoto;

import javax.security.auth.login.LoginException;

/**
 * Mock para conexão com autorizador
 */
public class MockServicoRemoto implements ServicoRemoto {
    String numeroConta;
    ContaCorrente contaCorrente;
    Integer saldoInicial = 0;

    /**
     * Preenche numero da conta valida
     * @param numeroConta
     */
    public MockServicoRemoto(final String numeroConta) {
        this.numeroConta = numeroConta;
    }

    public void setNumeroConta(final String numeroConta) {
        this.numeroConta = numeroConta;
    }

    /**
     * Set para saldo inical da conta corrente
     * @param saldo Valor do saldo
     */
    public void setSaldoInicial(final Integer saldo) {
        this.saldoInicial = saldo;
    }

    public ContaCorrente recuperarConta(String numeroConta) throws LoginException {
        if (numeroConta.equals(this.numeroConta)) {
            this.contaCorrente = new ContaCorrente(saldoInicial);
            return this.contaCorrente;
        }
        throw new LoginException("Conta não encontrada");
    }

    public void persistirConta(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }
}
