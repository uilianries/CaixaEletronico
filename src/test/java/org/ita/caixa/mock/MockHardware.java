package org.ita.caixa.mock;

import org.ita.caixa.hardware.Hardware;
import org.ita.caixa.exception.*;

/**
 * Realiza mock das chamadas de harware
 */
public class MockHardware implements Hardware {
    private String numeroConta;
    private boolean exception;

    /**
     * Preenche PAN padrão e desliga exceção de hardware
     */
    public MockHardware() {
        this.numeroConta = "4241672422447406";
        this.exception = false;
    }

    /**
     * Seleciona novo PAN
     * @param numeroConta PAN do portado
     */
    public void setNumedaContaCartao(final String numeroConta) {
        this.numeroConta = numeroConta;
    }

    /**
     * Habilita/Desabilita exceção de hardware
     * @param habilitar true para habilitar exceção a qualquer hardware
     */
    public void setHardwareException(final boolean habilitar) {
        this.exception = habilitar;
    }

    public String pegarNumeroDaContaCartao() throws LeitoraCartaoException {
        if (this.exception) {
            throw new LeitoraCartaoException();
        }
        return this.numeroConta;
    }

    public void entregarDinheiro() throws DispensadorException {
        if (this.exception) {
            throw new DispensadorException();
        }
    }

    public void lerEnvelope() throws LeitoraEnvolpeException {
        if (this.exception) {
            throw new LeitoraEnvolpeException();
        }
    }
}
