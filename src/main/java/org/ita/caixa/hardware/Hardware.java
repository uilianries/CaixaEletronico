package org.ita.caixa.hardware;

import org.ita.caixa.exception.DispensadorException;
import org.ita.caixa.exception.LeitoraCartaoException;
import org.ita.caixa.exception.LeitoraEnvolpeException;

/**
 * Interface para representaçao camada de HAL (Hardware Abstract Level).
 */
public interface Hardware {

    /**
     * Realiza leitura do cartão ao portador
     * @throws LeitoraCartaoException
     * @return PAN (Primary Account Number)
     */
    public String pegarNumeroDaContaCartao() throws LeitoraCartaoException;

    /**
     * Dispensa distribução de cédulas
     * @throws DispensadorException
     */
    public void entregarDinheiro() throws DispensadorException;

    /**
     * Processa envelope de depósito
     * @throws LeitoraEnvolpeException
     */
    public void lerEnvelope() throws LeitoraEnvolpeException;
}