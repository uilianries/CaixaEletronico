package org.ita.caixa.atm;

import org.ita.caixa.exception.HardwareException;
import org.ita.caixa.hardware.Hardware;
import org.ita.caixa.servico.*;

import javax.security.auth.login.LoginException;

public class CaixaEletronico {

    private Hardware hardware;
    private ServicoRemoto servicoRemoto;
    private ContaCorrente contaCorrente;

    public CaixaEletronico(Hardware hardware, ServicoRemoto servicoRemoto) {
        this.hardware = hardware;
        this.servicoRemoto = servicoRemoto;
    }

    public String logar() {
        try {
            String numeroConta = this.hardware.pegarNumeroDaContaCartao();
            this.contaCorrente = this.servicoRemoto.recuperarConta(numeroConta);
        } catch (HardwareException exception) {
            return "Ocorreu um erro durante o login: " + exception.getMessage();
        } catch (LoginException exception) {
            return "Não foi possível autenticar o usuário";
        }
        return "Usuário Autenticado";
    }

    public String saldo() {
        return "O saldo é R$ " + this.contaCorrente.getSaldo() + ",00";
    }

    public String sacar(Integer valor) {
        if (this.contaCorrente.getSaldo() < valor) {
            return "Saldo insuficiente";
        }

        try {
            this.hardware.entregarDinheiro();
        } catch (HardwareException exception) {
            return "Ocorreu um erro durante o saque: " + exception.getMessage();
        }

        this.contaCorrente.resgatar(valor);
        this.servicoRemoto.persistirConta(this.contaCorrente);
        return "Retire seu dinheiro";
    }

    public String depositar(Integer valor) {
        try {
            this.hardware.lerEnvelope();
        } catch (HardwareException exception) {
            return "Ocorreu um erro durante o depósito: " + exception.getMessage();
        }

        this.contaCorrente.investir(valor);
        this.servicoRemoto.persistirConta(this.contaCorrente);
        return "Depósito recebido com sucesso";
    }
}