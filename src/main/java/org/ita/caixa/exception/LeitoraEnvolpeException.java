package org.ita.caixa.exception;

/**
 * Exceção no hardware de leitora para envelope
 */
public class LeitoraEnvolpeException extends HardwareException {
    public LeitoraEnvolpeException() {
        super("Envelope vazio");
    }
}
