package org.ita.caixa.exception;

/**
 * Exceção para dispensador de cédulas
 */
public class DispensadorException extends HardwareException {
    public DispensadorException() {
        super("Sem cédulas");
    }
}
