package org.ita.caixa.exception;

/**
 * Implementar um falha de hardware do ATM
 */
public class HardwareException extends Exception {
    public HardwareException(String message) {
        super(message);
    }
}