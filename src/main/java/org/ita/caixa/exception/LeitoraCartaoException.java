package org.ita.caixa.exception;

/**
 * Exceção para leitora de cartão
 */
public class LeitoraCartaoException extends HardwareException {
    public LeitoraCartaoException() {
        super("Cartão clonado");
    }
}
