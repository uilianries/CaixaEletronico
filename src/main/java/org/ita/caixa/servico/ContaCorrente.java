package org.ita.caixa.servico;

/**
 * Representação de uma conta corrente bancária
 */
public class ContaCorrente {
    private Integer saldo;

    /**
     * Seta valor de saldo
     * @param saldo Valor do saldo inicial
     */
    public ContaCorrente(Integer saldo) {
        this.saldo = saldo;
    }

    /**
     * Resgata valor da conta correte
     * @param valor Valor que será resgatado
     */
    public void resgatar(Integer valor) {
        this.saldo -= valor;
    }

    /**
     * Investe novo valor sobre saldo da conta
     * @param valor Valor que será adicionado a conta
     */
    public void investir(Integer valor) {
        this.saldo += valor;
    }

    /**
     * Recupera o saldo atual
     * @return Valor do saldo atual
     */
    public Integer getSaldo() {
        return this.saldo;
    }
}
