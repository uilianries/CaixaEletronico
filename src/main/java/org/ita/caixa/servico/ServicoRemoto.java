package org.ita.caixa.servico;

import javax.security.auth.login.LoginException;

/**
 * Conexão com autoriazdor.
 */
public interface ServicoRemoto {

    /**
     * Requisita conta corrente para autorizador
     * @param numeroConta PAN do portador
     * @return Conta corrente do portador
     * @throws LoginException Se conta for inexistente
     */
    public ContaCorrente recuperarConta(final String numeroConta) throws LoginException;

    /**
     * Atualiza saldo remoto para portador
     * @param contaCorrente Conta atualizado para sincronizar
     */
    public void persistirConta(final ContaCorrente contaCorrente);
}
